import database from './data.json';

const data = database.product;

const service = {
    query: cond => {
        console.log('mock service product.query', cond);
        const ret = data.filter(product => product.mainCategory === cond.categoryId).map(i=>Object.assign({},i));
        return Promise.resolve({ data: ret });
    },
    update: product => {
        console.log('mock service product.update', product);
        var found = data.find(item => item.id === product.id);
        return found ? Promise.resolve({ data: Object.assign({},Object.assign(found, product))}) : Promise.reject({ data: product});
    },
    read: id => {
        console.log('mock service product.read', id);
        var found = data.find(item => item.id === id);
        return found ? Promise.resolve({ data: Object.assign({},found)}) : Promise.reject();
    },
    create: product => {
        console.log('mock service product.create', product);
        product.id = Date.now() % 100000000;
        data.push(product);
        return Promise.resolve({ data: Object.assign({},product)});
    },
    delete: product => {
        console.log('mock service product.delete', product);
        const index = data.findIndex(item => item.id === product.id);
        if (index > -1) {
            data.splice(index, 1);
        }
        return Promise.resolve({ data: product});
    }
}

export default service;