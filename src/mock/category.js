import database from './data.json';

const data = database.category;

const service = {
    query: (cond) => {
        console.log('mock service category.query', cond);
        const ret = data.filter(category => category.parent == cond.categoryId).map(i=>Object.assign({},i));
        return Promise.resolve({ data: ret });
    },
    update: category => {
        console.log('mock service category.update', category);
        var found = data.find(item => item.id === category.id);
        return found ? Promise.resolve({ data: Object.assign({},Object.assign(found, category))}) : Promise.reject({ data: category});
    },
    read: id => {
        console.log('mock service category.read', id);
        var found = data.find(item => item.id === id);
        return found ? Promise.resolve({ data: Object.assign({},found)}) : Promise.reject();
    },
    create: category => {
        console.log('mock service category.create', category);
        category.id = Date.now() % 100000000;
        data.push(category);
        return Promise.resolve({ data: Object.assign({},category)});
    },
    delete: category => {
        console.log('mock service category.delete', category);
        const index = data.findIndex(item => item.id === category.id);
        if (index > -1) {
            data.splice(index, 1);
        }
        return Promise.resolve({ data: category});
    }
}

export default service;