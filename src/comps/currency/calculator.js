import React, { Component, PureComponent } from 'react';
import Draggable from 'react-draggable';
import service from '../../service/currency-exchange';
import styles from './currency.module.scss';
import { IoIosCalculator , IoMdClose} from 'react-icons/io';

class CalculatorPanel extends Component {

    constructor(props) {
        super(props);
        this.state = {
            rates: props.rates,
            source:0,
            sourceCurrency:'EUR',
            target:0,
            targetCurrency:'EUR',
        }
        this.onRequestClose = props.onRequestClose;
    }

    handleChange(name,value){
        let state=Object.assign({},this.state);
        console.log('state before',state);
        state[name]=value;
        let {source,sourceCurrency,targetCurrency,rates} = state;
        console.log('state before cal',state);
        if(!isNaN(parseFloat(source))) {
            let target = source / rates[sourceCurrency] * rates[targetCurrency];
            state.target=parseFloat(target.toFixed(2));
        }
        console.log('state after cal',state);
        this.setState(state);
    }

    render() {
        return (
            <Draggable defaultPosition={{x:320,y:-60}} handle={'.' + styles.title}>
                <div className={'container-fluid ' + styles.calculator}>
                    <div className="row">
                        <div className={'col-12 ' + styles.title} style={{cursor: 'move'}}>
                            <div className="float-left">Exchange Calculator</div>
                            <div className="float-right">
                                <IoMdClose className={'pointer '+ styles.iconClose} onClick={this.onRequestClose}/>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-3">
                            <span className="float-left">from:</span>
                        </div>
                        <div className="col-6">
                            <input type="text" 
                                className="float-left"
                                onChange={(e) => this.handleChange('source', e.target.value)} 
                                value={this.state.source} />
                            &nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                        <div className="col-3">
                            <select onChange={(e) => this.handleChange('sourceCurrency', e.target.value)} value={this.state.sourceCurrency}>
                                <option value='EUR'>EUR</option>
                                <option value='USD'>USD</option>
                                <option value='GBP'>GBP</option>
                                <option value='CHF'>CHF</option>
                            </select>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-3">
                            <span className="float-left">to:</span>
                        </div>
                        <div className="col-6">
                            <span className="float-left">&nbsp;&nbsp;{this.state.target}</span>
                        </div>
                        <div className="col-3">
                            <select onChange={(e) => this.handleChange('targetCurrency', e.target.value)} value={this.state.targetCurrency}>
                                <option value='EUR'>EUR</option>
                                <option value='USD'>USD</option>
                                <option value='GBP'>GBP</option>
                                <option value='CHF'>CHF</option>
                            </select>
                        </div>
                    </div>
                </div>
            </Draggable>
        );
    }
}

class Calculator extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            rates: {}
        };
    }

    open() { 
        service.read().then(rates => {
            this.setState(Object.assign({},this.state,{open: true, rates:rates}));
        });
    }

    close() {
        this.setState({open: false});
    }

    render() {
        let dialog = null;
        if (this.state.open){
            dialog = <CalculatorPanel onRequestClose={()=>this.close()} rates={this.state.rates}/>
        }
        return (
            <div style={{display:'inline'}}>
                <span className="pointer" onClick={()=>this.open()}>
                    <IoIosCalculator />
                </span>
                {dialog}
            </div>
        );
    }
}

export default Calculator;