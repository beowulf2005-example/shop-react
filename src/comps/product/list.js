import React, { Component } from 'react';
import styles from './list.module.scss';
import {IoIosCloseCircle} from 'react-icons/io';
import Calulator from '../currency/calculator';

let service;
let model;
let local;
let productActions;

class ProductDetail extends Component {

  constructor(props) {
    super(props);
    this.state = {
      edit: false,
      show: false,
      product: {
        title: '',
        price: 0,
        currency: 'USD',
      },
    };
    productActions = {
      open: (prod) => this.open(prod),
      close: () => this.close(),
      eidt: () => this.edit(),
      save: () => this.save(),
    }
  }

  open(prod) {
    if (!prod.mainCategory) {
      return;
    }
    if (prod.id) {
      //open to view a existing prod,(read mode)
      service.read(prod.id).then((res) => {
        const product = res.data;
        this.setState({ edit: false, show: true, product: product });
      });
    } else {
      //open to create product,(edit mode)
      this.setState({ edit: true, show: true, product: prod });
    }
  }

  close() {
    this.setState({ show: false });
  }

  edit() {
    this.setState({ edit: true });
  }

  save() {
    const product = this.state.product;
    console.log('save product', product);
    if (this.validate(product)) {
      console.log('validated product', product);
      product.price = Number.parseFloat(product.price);
      (product.id ? service.update(product) : service.create(product)).then((resp) => {
        const result = resp.data;
        this.setState({ edit: false, product: result });
        local.reload = true;
        local.render();
      });
    }
  }

  validate(prod) {
    return (typeof prod.title) === 'string'
      && prod.title.trim().length > 3
      && Number.parseFloat(prod.price) > 0
      && (typeof prod.currency) === 'string'
      && prod.currency.trim().length >= 3;
  }

  handleChange(propName, e) {
    if (this.state.edit) {
      const product = Object.assign({}, this.state.product);
      product[propName] = e.target.value;
      this.setState({ product: product });
    }
  }

  render() {
    const product = this.state.product;
    const show = this.state.show;
    const edit = this.state.edit;
    const inputStyle = edit ? styles.editable : styles.readonly;
    return (
      <div className={`${styles.mask} ${show ? styles.show : styles.hide}`}>
        <div className={styles.dialog}>
          <div>categoryId: {product.id ? product.id : '<new>'}</div>
          <div>mainCategory: {product.mainCategory ? product.mainCategory : '<error>'}</div>
          <div>title:</div>
          <textarea className={inputStyle}
            onChange={(e) => this.handleChange('title', e)} value={product.title} />          
          <br />   
          <br />
          <div>price:
            <input type="text" className={inputStyle}
              onChange={(e) => this.handleChange('price', e)} value={product.price} />
          </div>
          <br />
          <div>currency:
            <select onChange={(e) => this.handleChange('currency', e)} value={product.currency}>
              <option value='EUR'>EUR</option>
              <option value='USD'>USD</option>
              <option value='GBP'>GBP</option>
              <option value='CHF'>CHF</option>
            </select>
            <Calulator/>
          </div>
          <br />
          <div className={styles.inline}>
            {edit?<button className='btn btn-primary' onClick={() => this.save()}>Save</button>
            :<button className='btn btn-primary' onClick={() => this.edit()}>Edit</button>}
            &nbsp;&nbsp;
            <button className={styles.inline + ' btn btn-primary'} onClick={() => this.close()}>Close</button>
          </div>
        </div>
      </div>
    );
  }
}

class Product extends Component {

  open() {
    productActions.open(this.props.product);
  }

  delete(){
    service.delete(this.props.product).then(()=>{
      local.reload = true;
      local.render();
    });
  }

  render() {
    return (
      <div className="row">
        <div className="col-8 pointer underline" onClick={() => this.open()}>{this.props.product.title}</div>
        <div className="col-2" >{this.props.product.price}</div>
        <div className="col-1" >{this.props.product.currency}</div>
        <div className="col-1 pointer" ><IoIosCloseCircle onClick={()=>this.delete()}/></div>
      </div>
    );
  }
}
class ProductList extends Component {

  constructor(props) {
    super(props);
    console.log('constructor ProductList', props);
    service = props.service;
    model = props.model;
    this.reload = false;
    this.state = {
      categoryId: null,
      products: [],
    }
    local = this;
  }

  load() {
    let catId = model.state.selectedCategoryId;
    service.query({ categoryId: catId }).then(res => {
      const products = res.data;
      console.log(products);
      this.setState({ products: products, categoryId: catId });
      return Promise.resolve();
    });
  }

  create() {
    const product = { id: null, mainCategory: this.state.categoryId, title: '', price: 0, currency: 'EUR' };
    productActions.open(product);
  }

  render() {
    if (this.state.categoryId !== model.state.selectedCategoryId || this.reload) {
      this.load();
      this.reload = false;
    }
    const list = this.state.products.map(prod => (<Product key={'key_prod_' + prod.id} product={prod} />));
    return (
      <div>
        <ProductDetail />
        <button className='btn btn-primary' onClick={() => this.create()}>new product in current category</button>
        <div className="container-fluid">
          <div className="row">
            <div className="col-8" >title</div>
            <div className="col-2" >price</div>
            <div className="col-1" >currency</div>
            <div className="col-1" >delete</div>
          </div>
          {list}
        </div>
      </div>
    );
  }
}

export default ProductList;
