import React, { Component } from 'react';
import styles from './tree.module.scss';
import {IoIosAddCircle,IoMdCreate, IoIosCloseCircle,IoIosAdd,IoIosRemove} from 'react-icons/io';

let service;
let model;
let local;
let reset;

class CategoryDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      edit: false,
    };
    this.titleInput = React.createRef();
    reset = (edit) => this.reset(edit);
  }

  reset(edit) {
    this.setState({ edit: edit });
  }

  load(requestId) {
    return service.read(requestId).then(res => {
      if (res.data) {
        local.setState({ form: { show: this.props.form.show, category: res.data } });
        return Promise.resolve(res.data);
      }
    });
  }

  close() {
    const cate = this.props.form.category;
    local.setState({ form: { show: false, category: cate } });
  }

  edit() {
    this.setState({ edit: true });
  }

  save() {
    const title = this.titleInput.current.innerHTML.trim();
    const cate = Object.assign({}, this.props.form.category, { title: title });
    console.log('save cate', cate);
    if (this.validate(cate)) {
      console.log('validated category', cate);
      (cate.id ? service.update(cate) : service.create(cate)).then((res) => {
        const cate = res.data;
        this.setState({ edit: false });
        const form = Object.assign(local.state.form, { category: cate });
        local.setState({ form: form, reload: cate.parent });
      });
    }
  }

  validate(cate) {
    return (typeof cate.title) === 'string' && cate.title.trim().length > 3;
  }

  handleChange(e) {
    if (this.state.edit) {
      const cate = Object.assign({}, this.props.form.category, { title: e.target.value });
      local.setState({ form: { show: true, category: cate } });
    }
  }

  render() {
    const cate = this.props.form.category;
    const edit = this.state.edit;
    return (
      <div className={`${styles.mask} ${this.props.form.show ? styles.show : styles.hide}`}>
        <div className={styles.dialog}>
          <div>categoryId: {cate.id ? cate.id : '<new>'}</div>
          <div>parent: {cate.parent ? cate.parent : '<root>'}</div>
          <div>title:</div>
          <textarea ref={this.titleInput}
            className={edit ? styles.editable : styles.readonly}
            onChange={(e) => this.handleChange(e)}
            value={cate.title} />
          <br/>
          <br/>
          <div className={styles.inline}>
            {edit?<button className='btn btn-primary' onClick={() => this.save()}>Save</button>
            :<button className='btn btn-primary' onClick={() => this.edit()}>Edit</button>}
            &nbsp;&nbsp;
            <button className='btn btn-primary' onClick={() => this.close()}>Close</button>
          </div>
        </div>
      </div>
    );
  }
}
class Category extends Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: props.expanded,
    };
  }

  toggle() {
    console.log('category toggle', this.state);
    if (!this.state.expanded) {
      this.setState({ expanded: true });
    } else {
      this.setState({ expanded: false });
    }
  }

  select() {
    console.log("category select", this.state, this.props);
    model.setState({ selectedCategoryId: this.props.category.id });
  }

  open() {
    reset(false);
    const cate = this.props.category;
    local.setState({ form: { show: true, category: cate } });
  }

  create() {
    reset(true);
    const cate = this.props.category;
    local.setState({ form: { show: true, category: { id: null, parent: cate.id, title: '' } } })
  }

  delete(){
    service.delete(this.props.category).then(()=>{
      local.setState({ reload: this.props.category.parent });
    });
  }

  render() {
    return (
      <li> 
        {this.state.expanded ? 
        <IoIosRemove className="pointer" onClick={() => this.toggle()}/>
        :<IoIosAdd className="pointer" onClick={() => this.toggle()}/>} 
        <span onClick={() => this.select()}
          className={`pointer underline ${model.state.selectedCategoryId === this.props.category.id ? styles.selected : ''}`}>{this.props.category.title}</span>
        <IoMdCreate className="pointer" onClick={() => this.open()}/>
        <IoIosAddCircle className="pointer" onClick={() => this.create()}/>
        <IoIosCloseCircle className="pointer" onClick={() => this.delete()}/>
        <CategoryList parent={this.props.category.id} expanded={this.state.expanded} />
      </li>
    );
  }

}

class CategoryList extends Component {

  constructor(props) {
    super(props);
    this.parent = props.parent;
    this.state = {
      categories: [],
      loaded: false,
    };
  }

  load() {
    service.query({ categoryId: this.parent }).then(res => {
      const cates = res.data;
      console.log(cates);
      this.setState({ categories: cates, loaded: true });
      return Promise.resolve();
    });
  }

  isReloadNeeded() {
    const ret = (local.state.reload !== -1) && (this.parent == local.state.reload);
    if (ret) {
      //consume reload
      console.log('reload once');
      local.state.reload = -1;
    }
    return ret;
  }

  render() {
    if ((this.props.expanded && !this.state.loaded) || this.isReloadNeeded()) {
      this.load();
    }
    const list = this.state.categories.map(cate =>
      (<Category key={'key_cate_' + cate.id}
        expanded={false}
        selected={model.state.selectedCategoryId === cate.id}
        category={cate} />));
    return (
      <ul className={`${styles.menu} ${this.props.expanded ? '' : styles.hide}`}>
        {list}
      </ul>
    );
  }
}

class CategoryTree extends Component {

  constructor(props) {
    super(props);
    console.log('constructor CategoryTree', props);
    service = props.service;
    model = props.model;
    this.state = {
      reload: -1,
      form: {
        show: false,
        category: { id: null, title: '', parent: null }
      }
    };
    local = this;
  }

  create() {
    reset(true);
    const cate = { id: null, title: '', parent: null };
    this.setState({ form: { show: true, category: cate } });
  }

  render() {
    return (
      <div>
        <CategoryDetail form={this.state.form} />
        <button className='btn btn-primary' onClick={() => this.create()}>new category in root</button>
        <CategoryList parent={null} expanded={true} />
      </div>
    )
  }
}

export default CategoryTree;
