
import config from '../config.json';
import axios from 'axios';

const service = {
    query: cond => axios.post(config.url_base + "category/query", cond),
    update: category => axios.put(config.url_base + "category/" + category.id, category),
    read: id => axios.get(config.url_base + "category/" + id),
    create: category => axios.post(config.url_base + "category/", category),
    delete: category => axios.delete(config.url_base + "category/" + category.id),
}

export default service;