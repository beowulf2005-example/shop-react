
import config from '../config.json';
import axios from 'axios';

const service = {
    query: cond => axios.post(config.url_base + "product/query", cond),
    update: product => axios.put(config.url_base + "product/" + product.id, product),
    read: id => axios.get(config.url_base + "product/" + id),
    create: product => axios.post(config.url_base + "product/", product),
    delete: product => axios.delete(config.url_base + "product/" + product.id),
}

export default service;