
import config from '../config.json';
import axios from 'axios';

const cache={
    timestamp: new Date().getMilliseconds(),
    rates: null,
    ttl:30*1000,
};

const service = {
    read: () => {
        let now = new Date().getMilliseconds();
        if (cache.rates && now-cache.timestamp<cache.ttl) {
            return Promise.resolve(cache.rates);
        } else {
            return axios.get(config.fixer_source).then(resp=>{
                let rates = resp.data.rates;
                rates["EUR"]=1.0;
                cache.timestamp=new Date().getMilliseconds();
                cache.rates=rates;
                return Promise.resolve(rates);
            });
        }
    }
}

// const service = {
//     read: ()=>{
//       return Promise.resolve({
//         "EUR": 1.0,
//         "USD": 1.139556,
//         "GBP": 0.895172,
//         "CHF": 1.124155
//       });
//     }
// }

export default service;