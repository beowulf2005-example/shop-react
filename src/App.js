import React, { Component } from 'react';
import './App.css';
import CategoryTree from './comps/category/tree';
import ProductList from './comps/product/list';
// import category from './mock/category';
// import product from './mock/product';
import category from './service/category';
import product from './service/product';

class App extends Component {

  constructor(props) {
    super(props);
    this.state={selectedCategoryId:null};
  }

  shouldComponentUpdate(props, state) {
    console.log('App shouldComponentUpdate', props, state);
    return true;
  }

  render() {
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-12 text-center">
            <header>Sample Shop</header>
          </div>
        </div>
        <div className="row">
          <div className="col-4">
            <aside><CategoryTree service={category} model={this}/></aside>
          </div>
          <div className="col-8">
            <ProductList service={product} model={this} categoryId={this.state.selectedCategoryId}/>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
